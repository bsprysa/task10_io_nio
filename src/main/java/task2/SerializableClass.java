package task2;

public class SerializableClass implements java.io.Serializable {

  private String string1;
  private transient String string2;

  public SerializableClass(String string1, String string2) {
    this.string1 = string1;
    this.string2 = string2;
  }

  public String getString1() {
    return string1;
  }

  public String getString2() {
    return string2;
  }
}
