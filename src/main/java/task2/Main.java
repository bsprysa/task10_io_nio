package task2;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

public class Main {

  public static void main(String[] args) {
    String string1 = "Epam";
    String string2 = "Epam";
    System.out.println("string1 = "+string1);
    System.out.println("string2 = "+string2);
    SerializableClass object = new SerializableClass(string1, string2);
    String filename = "file.ser";

    // Serialization
    try {
      FileOutputStream file = new FileOutputStream(filename);
      ObjectOutputStream out = new ObjectOutputStream(file);
      out.writeObject(object);
      out.close();
      file.close();
      System.out.println("Object has been serialized");
    } catch (IOException e) {
      e.printStackTrace();
    }

    SerializableClass object1 = null;

    // Deserialization
    try {
      FileInputStream file = new FileInputStream(filename);
      ObjectInputStream in = new ObjectInputStream(file);
      object1 = (SerializableClass) in.readObject();
      in.close();
      file.close();
      System.out.println("Object has been deserialized");
      System.out.println("string1 = " + object1.getString1());
      System.out.println("string2 (transient) = " + object1.getString2());
    } catch (Exception e) {
      e.printStackTrace();
    }
  }
}
