package task6;

import java.io.File;
import java.text.DateFormat;
import java.text.SimpleDateFormat;

public class DisplayTheContentsOfADirectory {

  public static void main(String[] args) {
    File directory = new File("C:\\Users\\Bogdan\\IdeaProjects\\task10_IO_NIO");
    File[] contentsOfADirectory = directory.listFiles();
    DateFormat o = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
    for (File f : contentsOfADirectory) {
      if (f.isFile()) {
        System.out.println(
            "File name: " + f.getName() + " (lastModified: " + o.format(f.lastModified()) + ")");
      } else if (f.isDirectory()) {
        System.out.println(
            "Directory name: " + f.getName() + " (lastModified: " + o.format(f.lastModified())
                + ")");
      }
    }
  }

}
