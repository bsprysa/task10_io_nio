package task3;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Arrays;

public class Main {

  private static void testFileSize(int mb) throws IOException {
    File file = File.createTempFile("test", ".txt");
    file.deleteOnExit();
    char[] chars = new char[1024];
    Arrays.fill(chars, 'A');
    String longLine = new String(chars);
    long start1 = System.currentTimeMillis();
    PrintWriter pw = new PrintWriter(new FileWriter(file));
    for (int i = 0; i < mb * 1024; i++)
      pw.println(longLine);
    pw.close();
    long time1 = System.currentTimeMillis() - start1;
    System.out.println("Write: "+time1+" Millis");

    long start2 = System.currentTimeMillis();
    BufferedReader br = new BufferedReader(new FileReader(file));
    for (String line; (line = br.readLine()) != null; ) {
    }
    br.close();
    long time2 = System.currentTimeMillis() - start2;
    System.out.println("Read: "+time2+" Millis");
    file.delete();
  }

  public static void main(String[] args) throws IOException {
      testFileSize(100);
  }

}
